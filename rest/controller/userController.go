package controller

import (
	"net/http"

	"github.com/andykwanfai/rest/dao"
	. "github.com/andykwanfai/rest/model"
	"github.com/ant0ine/go-json-rest/rest"
)

func GetAllUsers(w rest.ResponseWriter, r *rest.Request) {
	users := dao.GetAllUsers()
	w.WriteJson(&users)
}

func CreateUser(w rest.ResponseWriter, r *rest.Request) {
	user := User{}
	err := r.DecodeJsonPayload(&user)
	if err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = dao.CreateUser(user)

	if err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
