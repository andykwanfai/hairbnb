package dao

import (
	"fmt"
	"log"
	"reflect"

	"github.com/andykwanfai/rest/constant"
	. "github.com/andykwanfai/rest/model"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

const (
	collection = constant.UsersCollection
	db         = constant.DB
	host       = constant.HOST
)

func getSession() *mgo.Session {
	session, err := mgo.Dial(host)
	if err != nil {
		panic(err)
	}

	return session
}

func GetAllUsers() (users []User) {
	session := getSession()
	defer session.Close()
	c := session.DB(db).C(collection)

	result := []User{}
	fmt.Println(reflect.TypeOf(result))
	err := c.Find(bson.M{}).All(&result)
	if err != nil {
		log.Fatal(err)
	}
	return result
}

func CreateUser(user User) error {
	session := getSession()
	defer session.Close()
	c := session.DB(db).C(collection)
	err := c.Insert(user)
	if err != nil {
		return err
	}
	return nil
}
