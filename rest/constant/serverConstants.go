package constant

const (
	HOST            = "0.0.0.0"
	DB              = "local"
	UsersCollection = "users"
)
